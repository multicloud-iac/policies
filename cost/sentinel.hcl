# Import Functions

module "tfplan-functions" {
  source = "../functions/tfplan-functions.sentinel"
}

module "tfstate-functions" {
  source = "../functions/tfstate-functions.sentinel"
}

module "tfconfig-functions" {
  source = "../functions/tfconfig-functions.sentinel"
}

module "tfrun-functions" {
  source = "../functions/tfrun-functions.sentinel"
}

module "aws-functions" {
  source = "../functions/aws-functions.sentinel"
}

module "azure-functions" {
  source = "../functions/azure-functions.sentinel"
}

# Block using cost estimates for cost and percent increase

policy "limit-cost-and-percentage-increase" {
  source = "./limit-cost-and-percentage-increase.sentinel"
  enforcement_level = "hard-mandatory"
}

# Restrict Instance/VM sizes on AWS and Azure

policy "restrict-ec2-instance-type" {
  source = "./restrict-ec2-instance-type.sentinel"
  enforcement_level = "soft-mandatory"
}

policy "restrict-vm-size-azure" {
  source = "./restrict-vm-size-azure.sentinel"
  enforcement_level = "soft-mandatory"
}

# Enforce Mandatory Tags on AWS and Azure

policy "enforce-mandatory-tags-aws" {
  source = "./enforce-mandatory-tags-aws.sentinel"
  enforcement_level = "soft-mandatory"
}

policy "enforce-mandatory-tags-azure" {
  source = "./enforce-mandatory-tags-azure.sentinel"
  enforcement_level = "soft-mandatory"
}