# Import Functions

module "tfplan-functions" {
  source = "../functions/tfplan-functions.sentinel"
}

module "tfstate-functions" {
  source = "../functions/tfstate-functions.sentinel"
}

module "tfconfig-functions" {
  source = "../functions/tfconfig-functions.sentinel"
}

module "aws-functions" {
  source = "../functions/aws-functions.sentinel"
}

module "azure-functions" {
  source = "../functions/azure-functions.sentinel"
}

# Restrict open CIDR blocks on AWS and Azure

policy "restrict-aws-ingress-sg-rule-cidr-blocks" {
  source = "./restrict-aws-ingress-sg-rule-cidr-blocks.sentinel"
  enforcement_level = "soft-mandatory"
}

policy "restrict-azure-inbound-source-address-prefixes" {
  source = "./restrict-azure-inbound-source-address-prefixes.sentinel"
  enforcement_level = "soft-mandatory"
}

# Restrict AMI publishers on AWS and Azure

policy "restrict-aws-ami-owners" {
  source = "./restrict-aws-ami-owners.sentinel"
  enforcement_level = "soft-mandatory"
}

policy "restrict-azure-vm-publisher" {
    source = "./restrict-azure-vm-publisher.sentinel"
    enforcement_level = "soft-mandatory"
}